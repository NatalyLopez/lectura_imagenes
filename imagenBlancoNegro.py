import cv2 as cv


#leer imagen con opencv
imagen = cv.imread('../Externo/dibujo.jpg') 

grayImage = cv.cvtColor(imagen, cv.COLOR_BGR2GRAY)
#grayImage = cv.imread('./Imagenes/lena.png',0) # otra forma de convertir a escala grices

(thresh, blackAndWhiteImage) = cv.threshold(grayImage, 75, 255, cv.THRESH_BINARY)

#muestra pantalla con titulo
cv.imshow('Black white image', blackAndWhiteImage)
cv.imwrite('./Salida/img_bn.png', blackAndWhiteImage) # guarda la imagen en un directorio interno
cv.imshow('Original image',imagen)
cv.imwrite('./Salida/img_or.png', imagen)
cv.imshow('Gray image', grayImage)
cv.imwrite('./Salida/img_gray.png', grayImage)
#espera para que la ventana se destruya
cv.waitKey(0)
#cierra la ventana
cv.destroyAllWindows()