import cv2 as cv


#leer imagen con opencv
imagen = cv.imread('./Imagenes/lena.png')
#muestra pantalla con titulo
cv.imshow('Lena',imagen)
#espera para que la ventana se destruya
cv.waitKey(0)
#cierra la ventana
cv.destroyAllWindows()
